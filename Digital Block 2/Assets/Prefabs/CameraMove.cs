﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public bool zoom;
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        zoom = true;
    }

    // Update is called once per frame
    void Update()
    {
       // transform.position = new Vector3(-243, player.transform.position.y + 60, player.transform.position.z - 20);

        if (player.transform.position.y >= 60 & zoom == true)
        {
            this.gameObject.GetComponent<Animator>().Play("zoom out");
            zoom = false;
        }

        if (player.transform.position.y < 60 & zoom == false)
        {
            this.gameObject.GetComponent<Animator>().Play("zoom in");
            zoom = true;
        }
    }
}
