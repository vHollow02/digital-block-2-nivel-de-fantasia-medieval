﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MouseManager : MonoBehaviour
{
    public LayerMask clickableLayer;

    public Camera cam;

    public Texture2D pointer;
    public Texture2D clickable;
    public Texture2D door;
    public Texture2D attack;

    public EventVector3 OnClickEnvironment;


    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, 1000, clickableLayer.value))
        {

           
            if (hit.collider.gameObject.tag == "Door")
            {
                Cursor.SetCursor(door, new Vector2(16, 16), CursorMode.Auto);              
               
            }
            else
            {
                Cursor.SetCursor(clickable, new Vector2(16, 16), CursorMode.Auto);
            }

            if (Input.GetMouseButtonDown(0))
            {
                OnClickEnvironment.Invoke(hit.point);
            }
        }
        else
        {
            Cursor.SetCursor(pointer, Vector2.zero, CursorMode.Auto);
        }
    }
}

[System.Serializable]
public class EventVector3 : UnityEvent<Vector3> { }
